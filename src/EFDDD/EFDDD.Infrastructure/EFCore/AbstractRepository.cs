﻿namespace EFDDD.Infrastructure.EFCore
{
    public abstract class AbstractRepository<T> where T : class
    {
        protected readonly FreeDbContext _dbContext;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        public AbstractRepository(FreeDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <inheritdoc/>
        public IEnumerable<T> GetAll()
        {
            return _dbContext.Set<T>()
                .ToList();
        }

        /// <inheritdoc/>
        public void Add(T entity)
        {
            _dbContext.Set<T>()
                .Add(entity);
        }
    }
}