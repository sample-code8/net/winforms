﻿using EFDDD.Domain.Entities;
using EFDDD.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;

namespace EFDDD.Infrastructure.EFCore;

/// <summary>
/// FreeDBコンテキストクラス
/// </summary>
public partial class FreeDbContext : DbContext
{
    public FreeDbContext(DbContextOptions<FreeDbContext> options)
        : base(options)
    {
    }

    /// <summary>製品テーブル</summary>
    public virtual DbSet<ProductEntity> Products { get; set; }

    /// <summary>製品アイテムテーブル</summary>
    public virtual DbSet<ProductItemEntity> ProductItems { get; set; }

    /// <summary>ログテーブル</summary>
    public virtual DbSet<LogEntity> Logs { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .HasDefaultSchema("FREE")
            .UseCollation("USING_NLS_COMP");

        // 製品テーブル
        modelBuilder.ApplyConfiguration(new ProductConfiguration());

        // 製品アイテムテーブル
        modelBuilder.Entity<ProductItemEntity>(entity =>
        {
            // 主キー
            entity.HasKey(e => new { e.ProductId, e.ProductItemNo })
                .HasName("PRODUCT_ITEMS_PKC");

            // 外部キー
            entity.HasOne(d => d.Product)
                .WithMany(p => p.ProductItems)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("PRODUCT_ITEMS_FK1");

            // 製品ID
            entity.Property(e => e.ProductId)
                .HasConversion(p => p.Value, p => new ProductId(p));

            // 金額と通貨
            entity.OwnsOne(p => p.GlobalPrice, g =>
            {
                g.Property(p => p.Amount).HasColumnName("AMOUNT");
                g.Property(p => p.Currency).HasColumnName("CURRENCY");
            });
        });

        // ログテーブル
        modelBuilder.Entity<LogEntity>(entity =>
        {
            // ログID（主キー）
            entity.HasKey(e => e.LogId)
                .HasName("LOGS_PKC");
            entity.Property(e => e.LogId)
                .ValueGeneratedOnAdd();
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}