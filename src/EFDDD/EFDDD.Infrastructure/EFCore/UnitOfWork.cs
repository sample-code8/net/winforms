﻿using EFDDD.Domain.Repositories;

namespace EFDDD.Infrastructure.EFCore
{
    public sealed class UnitOfWork
        : IUnitOfWork
    {
        private readonly FreeDbContext _dbContext;
        private IProductRepository _productRepository;
        private ILogRepository _logRepository;

        public UnitOfWork(FreeDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IProductRepository ProductRepository
        {
            get
            {
                if (_productRepository == null)
                {
                    _productRepository = new ProductRepository(_dbContext);
                }
                return _productRepository;
            }
        }

        public ILogRepository LogRepository
        {
            get
            {
                if (_logRepository == null)
                {
                    _logRepository = new LogRepository(_dbContext);
                }
                return _logRepository;
            }
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}