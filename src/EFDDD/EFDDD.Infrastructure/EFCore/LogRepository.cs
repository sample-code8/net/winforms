﻿using EFDDD.Domain.Entities;
using EFDDD.Domain.Repositories;

namespace EFDDD.Infrastructure.EFCore
{
    /// <summary>
    /// ログレポジトリ
    /// </summary>
    public class LogRepository
        : AbstractRepository<LogEntity>, ILogRepository
    {
        public LogRepository(FreeDbContext dbContext)
            : base(dbContext)
        {
        }
    }
}