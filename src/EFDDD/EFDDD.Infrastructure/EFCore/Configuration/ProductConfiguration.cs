﻿using EFDDD.Domain.Entities;
using EFDDD.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFDDD.Infrastructure.EFCore.Configuration;

public partial class FreeDbContext
{
    /// <summary>
    /// 製品テーブルの設定
    /// </summary>
    public class ProductConfiguration
        : IEntityTypeConfiguration<ProductEntity>
    {
        public void Configure(EntityTypeBuilder<ProductEntity> builder)
        {
            // 製品ID
            builder.HasKey(e => e.ProductId)
                    .HasName("PRODUCTS_PKC");
            builder.Property(e => e.ProductId)
                    .ValueGeneratedOnAdd()
                    .HasConversion(p => p.Value, p => new ProductId(p));

            // 単価
            builder.Property(e => e.UnitPrice)
                    .HasConversion(p => p.Value, p => new UnitPrice(p));
        }
    }
}