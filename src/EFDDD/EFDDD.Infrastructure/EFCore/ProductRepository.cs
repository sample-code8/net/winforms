﻿using EFDDD.Domain.Entities;
using EFDDD.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace EFDDD.Infrastructure.EFCore
{
    /// <summary>
    /// 製品レポジトリ
    /// </summary>
    public class ProductRepository
        : AbstractRepository<ProductEntity>, IProductRepository
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        public ProductRepository(FreeDbContext dbContext)
            : base(dbContext)
        {
        }

        /// <inheritdoc/>
        public IEnumerable<ProductEntity> GetAllWithItems()
        {
            return _dbContext.Set<ProductEntity>()
                .Include(p => p.ProductItems)
                .ToList();
        }
    }
}