﻿using EFDDD.Domain.Repositories;
using EFDDD.Infrastructure.EFCore;
using EFDDD.WinForm.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Oracle.ManagedDataAccess.Client;

namespace EFDDD.WinForm
{
    internal static class DI
    {
        private static ServiceCollection _services = new();
        private static ServiceProvider _serviceProvider;

        static DI()
        {
            // DB接続先
            var connectionStringBuilder = new OracleConnectionStringBuilder();
            connectionStringBuilder.DataSource = "localhost:1521/oracle";
            connectionStringBuilder.UserID = "FREE";
            connectionStringBuilder.Password = "password";

            _services.AddDbContext<FreeDbContext>(
                o => o.UseOracle(connectionStringBuilder.ConnectionString));

            _services.AddTransient<IUnitOfWork, UnitOfWork>();
            _services.AddTransient<Form1ViewModel>();

            //_services.AddScoped<IUnitOfWork, UnitOfWork>();
            //_services.AddScoped<Form1ViewModel>();

            _serviceProvider = _services.BuildServiceProvider();
        }

        internal static T Resolve<T>() where T : notnull
        {
            return _serviceProvider.GetRequiredService<T>();
        }
    }
}