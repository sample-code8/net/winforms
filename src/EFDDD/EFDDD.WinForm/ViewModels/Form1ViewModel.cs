﻿using System.ComponentModel;
using EFDDD.Domain.Entities;
using EFDDD.Domain.Repositories;
using PropertyChanged;

namespace EFDDD.WinForm.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class Form1ViewModel
    {
        private readonly IUnitOfWork _unitOfWork;

        public string ViewText { get; set; }
        public BindingList<Form1Product> Products { get; } = new();
        public BindingList<Form1ProductItem> ProductItems { get; } = new();
        public BindingList<LogEntity> Logs { get; } = new();
        public string ProductIdTextBoxText { get; set; }
        public string ProductNameTextBoxText { get; set; }
        public string UnitPriceTextBoxText { get; set; }

        public Form1ViewModel(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

            foreach (var product in _unitOfWork.ProductRepository.GetAllWithItems())
            {
                Products.Add(new Form1Product(product));
            }

            foreach (var log in _unitOfWork.LogRepository.GetAll())
            {
                Logs.Add(log);
            }
        }

        internal void Save()
        {
            int unitPrice = Convert.ToInt32(UnitPriceTextBoxText);

            var product = new ProductEntity(ProductNameTextBoxText, unitPrice);

            _unitOfWork.ProductRepository.Add(product);
            _unitOfWork.LogRepository.Add(
                new LogEntity(DateTime.Now, $"{product.ProductName} Insert!"));

            _unitOfWork.SaveChanges();

            ViewText = "Saved!!!";
        }

        public void ProductDataGridView_SelectionChanged(Form1Product? dto)
        {
            ProductItems.Clear();

            if (dto == null)
            {
                return;
            }

            foreach (var item in dto.ProductItems)
            {
                ProductItems.Add(item);
            }
        }
    }
}