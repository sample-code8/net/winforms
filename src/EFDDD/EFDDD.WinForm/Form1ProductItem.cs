﻿using EFDDD.Domain.Entities;

namespace EFDDD.WinForm
{
    public sealed class Form1ProductItem
    {
        private readonly ProductItemEntity _productItemEntity;

        public Form1ProductItem(ProductItemEntity productItemEntity)
        {
            _productItemEntity = productItemEntity;
        }

        public string ProductId => _productItemEntity.ProductId.DisplayValue;

        public string ProductItemNo => _productItemEntity.ProductItemNo.ToString();

        public string ProductItemName => _productItemEntity.ProductItemName;

        //public string Amount => _productItemEntity.Amount.ToString();

        //public string Currency => _productItemEntity.Currency.ToString();

        public string GlobalPrice => _productItemEntity.GlobalPrice.DisplayValue;
    }
}