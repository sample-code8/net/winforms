﻿namespace EFDDD.WinForm
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ProductDataGridView = new DataGridView();
            ProductIdTextBox = new TextBox();
            ProductNameTextBox = new TextBox();
            UnitPriceTextBox = new TextBox();
            SaveButton = new Button();
            ProductItemDataGridView = new DataGridView();
            LogDataGridView = new DataGridView();
            ((System.ComponentModel.ISupportInitialize)ProductDataGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)ProductItemDataGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)LogDataGridView).BeginInit();
            SuspendLayout();
            // 
            // ProductDataGridView
            // 
            ProductDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            ProductDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            ProductDataGridView.Location = new Point(12, 12);
            ProductDataGridView.Name = "ProductDataGridView";
            ProductDataGridView.Size = new Size(376, 334);
            ProductDataGridView.TabIndex = 0;
            // 
            // ProductIdTextBox
            // 
            ProductIdTextBox.Location = new Point(12, 352);
            ProductIdTextBox.Name = "ProductIdTextBox";
            ProductIdTextBox.ReadOnly = true;
            ProductIdTextBox.Size = new Size(100, 25);
            ProductIdTextBox.TabIndex = 1;
            // 
            // ProductNameTextBox
            // 
            ProductNameTextBox.Location = new Point(118, 352);
            ProductNameTextBox.Name = "ProductNameTextBox";
            ProductNameTextBox.Size = new Size(189, 25);
            ProductNameTextBox.TabIndex = 1;
            // 
            // UnitPriceTextBox
            // 
            UnitPriceTextBox.Location = new Point(313, 352);
            UnitPriceTextBox.Name = "UnitPriceTextBox";
            UnitPriceTextBox.Size = new Size(100, 25);
            UnitPriceTextBox.TabIndex = 1;
            // 
            // SaveButton
            // 
            SaveButton.Location = new Point(419, 352);
            SaveButton.Name = "SaveButton";
            SaveButton.Size = new Size(75, 23);
            SaveButton.TabIndex = 2;
            SaveButton.Text = "保存";
            SaveButton.UseVisualStyleBackColor = true;
            // 
            // ProductItemDataGridView
            // 
            ProductItemDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            ProductItemDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            ProductItemDataGridView.Location = new Point(394, 12);
            ProductItemDataGridView.Name = "ProductItemDataGridView";
            ProductItemDataGridView.Size = new Size(376, 334);
            ProductItemDataGridView.TabIndex = 0;
            // 
            // LogDataGridView
            // 
            LogDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            LogDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            LogDataGridView.Location = new Point(12, 383);
            LogDataGridView.Name = "LogDataGridView";
            LogDataGridView.Size = new Size(758, 135);
            LogDataGridView.TabIndex = 0;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(781, 530);
            Controls.Add(SaveButton);
            Controls.Add(ProductNameTextBox);
            Controls.Add(UnitPriceTextBox);
            Controls.Add(ProductIdTextBox);
            Controls.Add(ProductItemDataGridView);
            Controls.Add(LogDataGridView);
            Controls.Add(ProductDataGridView);
            Name = "Form1";
            Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)ProductDataGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)ProductItemDataGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)LogDataGridView).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DataGridView ProductDataGridView;
        private TextBox ProductIdTextBox;
        private TextBox ProductNameTextBox;
        private TextBox UnitPriceTextBox;
        private Button SaveButton;
        private DataGridView ProductItemDataGridView;
        private DataGridView LogDataGridView;
    }
}
