﻿using EFDDD.WinForm.ViewModels;

namespace EFDDD.WinForm
{
    public partial class Form1 : Form
    {
        private readonly Form1ViewModel _viewModel = DI.Resolve<Form1ViewModel>();

        public Form1()
        {
            InitializeComponent();

            /*
             * データバインド
             */
            // データグリッドビュー
            ProductDataGridView.DataSource = _viewModel.Products;
            ProductItemDataGridView.DataSource = _viewModel.ProductItems;
            LogDataGridView.DataSource = _viewModel.Logs;

            // テキストボックス
            this.DataBindings.Add("Text", _viewModel, nameof(_viewModel.ViewText));
            ProductNameTextBox.DataBindings.Add("Text", _viewModel, nameof(_viewModel.ProductNameTextBoxText));
            UnitPriceTextBox.DataBindings.Add("Text", _viewModel, nameof(_viewModel.UnitPriceTextBoxText));

            // ボタンクリックイベント
            SaveButton.Click += (_, __) => _viewModel.Save();

            ProductDataGridView.SelectionChanged += (_, __) => _viewModel.ProductDataGridView_SelectionChanged(
                ProductDataGridView.CurrentRow.DataBoundItem as Form1Product);
        }
    }
}