﻿using EFDDD.Domain.Entities;

namespace EFDDD.WinForm
{
    public class Form1Product
    {
        private readonly ProductEntity _product;
        private readonly List<Form1ProductItem> _items = new();

        public Form1Product(ProductEntity product)
        {
            _product = product;

            foreach (var item in _product.ProductItems)
            {
                _items.Add(new Form1ProductItem(item));
            }
        }

        public string ProductId => _product.ProductId.DisplayValue;

        public string ProductName => _product.ProductName.ToString();

        public string UnitPrice => _product.UnitPrice.DisplayValueWithUnit;

        public ICollection<Form1ProductItem> ProductItems => _items;
    }
}