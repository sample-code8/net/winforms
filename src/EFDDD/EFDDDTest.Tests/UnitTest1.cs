﻿using EFDDD.Domain.Entities;
using EFDDD.Infrastructure.EFCore;
using EFDDD.WinForm.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace EFDDDTest.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FreeDbContext>();
            optionsBuilder.UseInMemoryDatabase("TestDatabase");
            optionsBuilder.EnableServiceProviderCaching(false);

            var dbContext = new FreeDbContext(optionsBuilder.Options);
            var unitOfWork = new UnitOfWork(dbContext);

            dbContext.Products.Add(new ProductEntity(1, "りんご", 100));
            dbContext.Products.Add(new ProductEntity(2, "みかん", 200));
            dbContext.SaveChanges();

            var vm = new Form1ViewModel(unitOfWork);
            Assert.AreEqual(2, vm.Products.Count());
        }

        [TestMethod]
        public void TestMethod2()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FreeDbContext>();
            optionsBuilder.UseInMemoryDatabase("TestDatabase");

            var dbContext = new FreeDbContext(optionsBuilder.Options);
            var unitOfWork = new UnitOfWork(dbContext);
            optionsBuilder.EnableServiceProviderCaching(false);

            dbContext.Products.Add(new ProductEntity(11, "りんご", 100));
            dbContext.Products.Add(new ProductEntity(12, "みかん", 200));
            dbContext.SaveChanges();

            var vm = new Form1ViewModel(unitOfWork);
            Assert.AreEqual(2, vm.Products.Count());
        }
    }
}