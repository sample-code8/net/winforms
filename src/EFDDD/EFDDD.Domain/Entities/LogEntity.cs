﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFDDD.Domain.Entities;

/// <summary>
/// ログエンティティクラス
/// </summary>
[Table("LOGS")]
public partial class LogEntity
{
    /// <summary>
    /// コンストラクタ
    /// </summary>
    private LogEntity(){}

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="logDatetime">ログ日付</param>
    /// <param name="logText">ログテキスト</param>
    public LogEntity(DateTime logDatetime, string logText)
    {
        LogDatetime = logDatetime;
        LogText = logText;
    }

    /// <summary>ログID</summary>
    [Key]
    [Column("LOG_ID", TypeName = "NUMBER")]
    public decimal LogId { get; }

    /// <summary>ログ日付</summary>
    [Column("LOG_DATETIME", TypeName = "DATE")]
    public DateTime LogDatetime { get; private set; }

    /// <summary>ログテキスト</summary>
    [Column("LOG_TEXT")]
    [StringLength(255)]
    public string LogText { get; private set; } = null!;
}