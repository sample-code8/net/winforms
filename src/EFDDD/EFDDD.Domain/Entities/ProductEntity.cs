﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EFDDD.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;

namespace EFDDD.Domain.Entities;

/// <summary>
/// 製品エンティティクラス
/// </summary>
[Table("PRODUCTS")]
public partial class ProductEntity
{
    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <remarks>
    /// EntityFrameworkを利用するには、引数なしのコンストラクタが必要。
    /// publicで公開すると引数なしでインスタンス生成（New）が可能で、
    /// これをさせないためprivateにして完全コンストラクタにする。
    /// </remarks>
    private ProductEntity()
    {
        // ここに処理を書くのはOK
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="productName">製品名</param>
    /// <param name="unitPrice">単価</param>
    /// <exception cref="ArgumentNullException">パラメーターがNullの場合、引数例外をスローします。</exception>
    public ProductEntity(string productName, int unitPrice)
        : this(0, productName, unitPrice)
    {
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="productId">製品ID</param>
    /// <param name="productName">製品名</param>
    /// <param name="unitPrice">単価</param>
    /// <exception cref="ArgumentNullException">パラメーターがNullの場合、引数例外をスローします。</exception>
    public ProductEntity(decimal productId, string productName, int unitPrice)
    {
        if (productId != 0)
        {
            ProductId = new ProductId(productId);
        }
        ProductName = productName ?? throw new ArgumentNullException(nameof(productName));
        UnitPrice = new UnitPrice(unitPrice) ?? throw new ArgumentNullException(nameof(unitPrice));
    }

    /// <summary>製品ID</summary>
    [Key]
    [Column("PRODUCT_ID", TypeName = "NUMBER")]
    public ProductId ProductId { get; private set; }

    /// <summary>製品名</summary>
    [Column("PRODUCT_NAME")]
    [StringLength(255)]
    public string ProductName { get; private set; } = null!;

    /// <summary>単価</summary>
    [Column("UNIT_PRICE")]
    [Precision(10)]
    public UnitPrice UnitPrice { get; private set; }

    /// <summary>製品アイテム情報</summary>
    [InverseProperty("Product")]
    public virtual ICollection<ProductItemEntity> ProductItems { get; private set; } = new List<ProductItemEntity>();
}