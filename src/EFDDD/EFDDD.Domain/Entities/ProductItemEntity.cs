﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EFDDD.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;

namespace EFDDD.Domain.Entities;

/// <summary>
/// 製品アイテムエンティティクラス
/// </summary>
[PrimaryKey("ProductId", "ProductItemNo")]
[Table("PRODUCT_ITEMS")]
public partial class ProductItemEntity
{
    /// <summary>
    /// コンストラクタ
    /// </summary>
    private ProductItemEntity()
    { }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="productId">製品ID</param>
    /// <param name="productItemNo">製品アイテムNo</param>
    /// <param name="productItemName">製品アイテム名</param>
    /// <param name="amount">金額</param>
    /// <param name="currency">通貨</param>
    public ProductItemEntity(
        ProductId productId, decimal productItemNo, string productItemName, decimal amount, string currency)
    {
        ProductId = productId;
        ProductItemNo = productItemNo;
        ProductItemName = productItemName;
        GlobalPrice = new GlobalPrice(amount, currency);
    }

    /// <summary>製品ID</summary>
    [Key]
    [Column("PRODUCT_ID", TypeName = "NUMBER")]
    public ProductId ProductId { get; private set; }

    /// <summary>製品アイテムNo</summary>
    [Key]
    [Column("PRODUCT_ITEM_NO", TypeName = "NUMBER")]
    public decimal ProductItemNo { get; private set; }

    /// <summary>製品アイテム名</summary>
    [Column("PRODUCT_ITEM_NAME")]
    [StringLength(255)]
    public string ProductItemName { get; private set; } = null!;

    public GlobalPrice GlobalPrice { get; }

    /// <summary>製品アイテム情報</summary>
    [ForeignKey("ProductId")]
    [InverseProperty("ProductItems")]
    public virtual ProductEntity Product { get; private set; } = null!;
}