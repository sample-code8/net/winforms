﻿namespace EFDDD.Domain.ValueObjects
{
    public abstract class AbstractValueObject<T> where T
        : AbstractValueObject<T>
    {
        public override bool Equals(object? obj)
        {
            var vo = obj as T;
            if (vo == null)
            {
                return false;
            }

            return EqualsCore(vo);
        }

        public static bool operator ==(AbstractValueObject<T> vo1, AbstractValueObject<T> vo2)
        {
            return Equals(vo1, vo2);
        }

        public static bool operator !=(AbstractValueObject<T> vo1, AbstractValueObject<T> vo2)
        {
            return !Equals(vo1, vo2);
        }

        protected abstract bool EqualsCore(T other);

        protected abstract int GetHashCodeCore();

        public override string ToString()
        {
            return base.ToString();
        }

        public override int GetHashCode()
        {
            return GetHashCodeCore();
        }
    }
}