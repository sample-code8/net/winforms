﻿namespace EFDDD.Domain.ValueObjects
{
    /// <summary>
    /// 単価の値オブジェクトクラス
    /// </summary>
    public sealed class ProductId : AbstractValueObject<ProductId>
    {
        public ProductId(decimal value)
        {
            Value = value;
        }

        public decimal Value { get; }

        public string DisplayValue => Value.ToString().PadLeft(6, '0');

        public override string ToString() => Value.ToString();

        protected override bool EqualsCore(ProductId other)
        {
            return Value == other.Value;
        }

        protected override int GetHashCodeCore()
        {
            return Value.GetHashCode();
        }
    }
}