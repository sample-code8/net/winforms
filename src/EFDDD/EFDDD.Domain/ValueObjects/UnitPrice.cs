﻿namespace EFDDD.Domain.ValueObjects
{
    /// <summary>
    /// 単価の値オブジェクトクラス
    /// </summary>
    public sealed class UnitPrice : AbstractValueObject<UnitPrice>
    {
        public const string UNIT = "円";

        public UnitPrice(int value)
        {
            Value = value;
        }

        public int Value { get; }

        public string DisplayValue => Value.ToString();
        public string DisplayValueWithUnit => Value + UNIT;

        public override string ToString() => Value.ToString();

        protected override bool EqualsCore(UnitPrice other)
        {
            return Value == other.Value;
        }

        protected override int GetHashCodeCore()
        {
            return Value.GetHashCode();
        }
    }
}