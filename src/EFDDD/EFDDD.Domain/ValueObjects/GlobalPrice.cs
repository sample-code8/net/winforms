﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace EFDDD.Domain.ValueObjects
{
    /// <summary>
    /// 単価の値オブジェクトクラス
    /// </summary>
    public sealed class GlobalPrice : AbstractValueObject<GlobalPrice>
    {
        public GlobalPrice(decimal amount, string currency)
        {
            Amount = amount;
            Currency = currency;
        }

        /// <summary>金額</summary>
        [Column("AMOUNT", TypeName = "NUMBER(18,2)")]
        public decimal Amount { get; }

        /// <summary>通貨</summary>
        [Column("CURRENCY")]
        [StringLength(50)]
        [Unicode(false)]
        public string Currency { get; } = null!;

        public string DisplayValue
        {
            get
            {
                if (Currency == "JPY")
                {
                    return decimal.Round(Amount) + "円";
                }
                else if (Currency == "USD")
                {
                    return "$" + decimal.Round(Amount);
                }

                return "?" + Amount;
            }
        }

        protected override bool EqualsCore(GlobalPrice other)
        {
            return Amount == other.Amount && Currency == other.Currency;
        }

        protected override int GetHashCodeCore()
        {
            return HashCode.Combine(Amount, Currency);
        }
    }
}