﻿using EFDDD.Domain.Entities;

namespace EFDDD.Domain.Repositories
{
    /// <summary>
    /// 製品レポジトリインターフェース
    /// </summary>
    public interface IProductRepository
    {
        /// <summary>
        /// 全製品データを取得します。
        /// </summary>
        /// <returns>全製品データ</returns>
        IEnumerable<ProductEntity> GetAll();

        /// <summary>
        /// 全製品データ(アイテム付き)を取得します。
        /// </summary>
        /// <returns>全製品データ</returns>
        IEnumerable<ProductEntity> GetAllWithItems();

        /// <summary>
        /// 製品を追加します。
        /// </summary>
        /// <param name="product">製品</param>
        void Add(ProductEntity product);
    }
}