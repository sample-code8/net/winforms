﻿using EFDDD.Domain.Entities;

namespace EFDDD.Domain.Repositories
{
    /// <summary>
    /// ログレポジトリインターフェース
    /// </summary>
    public interface ILogRepository
    {
        /// <summary>
        /// 全ログデータを取得します。
        /// </summary>
        /// <returns>全製品データ</returns>
        IEnumerable<LogEntity> GetAll();

        /// <summary>
        /// ログを追加します。
        /// </summary>
        /// <param name="Log">製品</param>
        void Add(LogEntity Log);
    }
}