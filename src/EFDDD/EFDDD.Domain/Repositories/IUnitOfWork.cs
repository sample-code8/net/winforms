﻿namespace EFDDD.Domain.Repositories
{
    public interface IUnitOfWork
    {
        IProductRepository ProductRepository { get; }

        ILogRepository LogRepository { get; }

        void SaveChanges();
    }
}