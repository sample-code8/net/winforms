﻿using System;
using NLog;

namespace NLSA.WinForms.Views
{
    public partial class MainView : AbstractView
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public MainView()
        {
            InitializeComponent();
        }

        private void SaveButton_Click(object sender, System.EventArgs e)
        {
            _logger.Info("保存ボタンが押下されました。");

            _logger.WithProperty("地域ID", AreaIdTextBox.Text)
                .WithProperty("地域名", AreaNameTextBox.Text)
                .Info("保存ボタンが押下されました。");

            var area = new { AreaId = 1, AreaName = "中国地方" };
            _logger.Info("保存ボタンが押下されました。{area}", area);
        }

        private void ThrowExceptionButton_Click(object sender, System.EventArgs e)
        {
            throw new NotImplementedException("ごめんなさい。未実装です。");
        }
    }
}