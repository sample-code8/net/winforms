﻿
namespace NLSA.WinForms.Views
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AreaIdTextBox = new System.Windows.Forms.TextBox();
            this.AreaNameTextBox = new System.Windows.Forms.TextBox();
            this.AreaIdLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.ThrowExceptionButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AreaIdTextBox
            // 
            this.AreaIdTextBox.Location = new System.Drawing.Point(96, 12);
            this.AreaIdTextBox.Name = "AreaIdTextBox";
            this.AreaIdTextBox.Size = new System.Drawing.Size(100, 25);
            this.AreaIdTextBox.TabIndex = 0;
            // 
            // AreaNameTextBox
            // 
            this.AreaNameTextBox.Location = new System.Drawing.Point(96, 43);
            this.AreaNameTextBox.Name = "AreaNameTextBox";
            this.AreaNameTextBox.Size = new System.Drawing.Size(235, 25);
            this.AreaNameTextBox.TabIndex = 0;
            // 
            // AreaIdLabel
            // 
            this.AreaIdLabel.AutoSize = true;
            this.AreaIdLabel.Location = new System.Drawing.Point(12, 15);
            this.AreaIdLabel.Name = "AreaIdLabel";
            this.AreaIdLabel.Size = new System.Drawing.Size(47, 17);
            this.AreaIdLabel.TabIndex = 1;
            this.AreaIdLabel.Text = "地域ID:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "地域名:";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(96, 87);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(235, 23);
            this.SaveButton.TabIndex = 2;
            this.SaveButton.Text = "保存";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ThrowExceptionButton
            // 
            this.ThrowExceptionButton.Location = new System.Drawing.Point(12, 87);
            this.ThrowExceptionButton.Name = "ThrowExceptionButton";
            this.ThrowExceptionButton.Size = new System.Drawing.Size(78, 23);
            this.ThrowExceptionButton.TabIndex = 2;
            this.ThrowExceptionButton.Text = "例外";
            this.ThrowExceptionButton.UseVisualStyleBackColor = true;
            this.ThrowExceptionButton.Click += new System.EventHandler(this.ThrowExceptionButton_Click);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 122);
            this.Controls.Add(this.ThrowExceptionButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AreaIdLabel);
            this.Controls.Add(this.AreaNameTextBox);
            this.Controls.Add(this.AreaIdTextBox);
            this.Name = "MainView";
            this.Text = "MainView";
            this.ViewName = "メイン画面";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox AreaIdTextBox;
        private System.Windows.Forms.TextBox AreaNameTextBox;
        private System.Windows.Forms.Label AreaIdLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button ThrowExceptionButton;
    }
}