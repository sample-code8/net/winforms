﻿using System;
using System.Windows.Forms;
using NLog;

namespace NLSA.WinForms.Views
{
    public partial class AbstractView : Form
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public AbstractView()
        {
            InitializeComponent();
        }

        public string ViewName { get; set; }

        private void AbstractView_Load(object sender, EventArgs e)
        {
            _logger.Info($"画面「{ViewName}」が起動されました。");
        }
    }
}