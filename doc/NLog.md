﻿# NLog

## NLogとは

`.NET`向けの柔軟で無料のロギングライブラリ。

!!! tip 複数のターゲットにログ出力可能
    - ファイル
    - コンソール
    - データベース
    - Email
    - イベントログ
    - デバッガー出力

!!! tip 設定が簡単
    構成ファイルとプログラムの両方を使用して設定するのが非常に簡単です。
    アプリケーションを再起動しなくても、構成を変更できます。

    NLog.config
    ```xml
    <!-- 書き込み先の設定 -->
    <targets>
        <!-- ファイル出力設定 -->
        <target xsi:type="File" name="logfile" fileName="c:\temp\console-example.log"
                layout="${longdate}|${level}|${message} |${all-event-properties} ${exception:format=tostring}" />
        <target xsi:type="Console" name="logconsole"
                layout="${longdate}|${level}|${message} |${all-event-properties} ${exception:format=tostring}" />
    </targets>

    <!-- 出力設定 -->
    <rules>
        <logger name="*" minlevel="Trace" writeTo="logfile,logconsole" />
    </rules>
    ```

!!! tip ログ出力内容のレイアウトが簡単
    さまざまな[レイアウトレンダリング](https://nlog-project.org/config/?tab=layout-renderers)がある。

!!! tip Microsoft 拡張ログ
    Microsoft Extensible Logging（およびASP.NET Core）と完全に統合でき、標準のMicrosoft LoggerFactoryを置き換える必要はありません。NLogは自動的にLogEventプロパティをキャプチャし、構造化されたロギングターゲット出力でそれらを使用することができます。

## 導入方法

1. `NuGet`より`NLog`をインストール
2. `NLog.config`ファイルの作成
※ プロパティで[出力ディレクトリにコピー]を"新しい場合はコピーする"にすること。

## サンプル

\src\NLogSampleApp

## 参考文献

- [NLog](https://nlog-project.org/)
